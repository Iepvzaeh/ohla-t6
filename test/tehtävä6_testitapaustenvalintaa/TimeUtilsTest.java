/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tehtävä6_testitapaustenvalintaa;

import java.util.Arrays;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TimeUtilsTest {
    
    private int aika;
    private String tulos;
    
    @Parameters    
    public static List testiTapaukset() {        
        return Arrays.asList(new Object[][] {            
            {0, "0:00:00"}, {-1, "-1"}, {59, "0:00:59"}, {60, "0:01:00"}, {61, "0:01:01"}, {3599, "0:59:59"}, {3600, "1:00:00"}, {3601, "1:00:01"}, {35999, "9:59:59"}, {36000, "-1"}       
        });    
    }
    
    public TimeUtilsTest(int aika, String tulos) {
        this.aika = aika;
        this.tulos = tulos;
    }

    /**
     * Test of secToTime method, of class TimeUtils.
     */
    @Test
    public void testSecToTime() {
        System.out.println("secToTime");
        int a = this.aika;
        String expResult = this.tulos;
        String result = TimeUtils.secToTime(a);
        assertEquals(expResult, result);
    }
    
}
